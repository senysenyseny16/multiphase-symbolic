from typing import Dict
from math import sqrt
from sympy import Symbol
from sympy import diff
from sympy import ImmutableMatrix, eye
from sympy import init_printing

init_printing()

alpha1 = Symbol('alpha1', real=True, positive=True)
rho1   = Symbol('rho1', real=True, positive=True)
rho2   = Symbol('rho2', real=True, positive=True)
F11    = Symbol('F11', real=True)
F12    = Symbol('F12', real=True)
F21    = Symbol('F21', real=True)
F22    = Symbol('F22', real=True)
u1     = Symbol('u1', real=True)

alpha2 = Symbol('alpha2', real=True, positive=True)
rho    = Symbol('rho', real=True, positive=True)

p1     = Symbol('p1', real=True)
p2     = Symbol('p2', real=True)

C1     = Symbol('C1', real=True, positive=True)
C2     = Symbol('C2', real=True, positive=True)
Cs     = Symbol('C_s', real=True, positive=True)

sigma11 = Symbol('sigma11', real=True)
sigma21 = Symbol('sigma21', real=True)

K11     = Symbol('K11', real=True)
K12     = Symbol('K12', real=True)
K21     = Symbol('K21', real=True)
K22     = Symbol('K22', real=True)

L11     = Symbol('L11', real=True)
L12     = Symbol('L12', real=True)
L21     = Symbol('L21', real=True)
L22     = Symbol('L22', real=True)

A0 = ImmutableMatrix([ [1, 0, 0, 0, 0, 0, 0, 0, 0]
                     , [rho1, alpha1, 0, 0, 0, 0, 0, 0, 0]
                     , [-rho2, 0, alpha2, 0, 0, 0, 0, 0, 0]
                     , [0, 0, 0, 1, 0, 0, 0, 0, 0]
                     , [0, 0, 0, 0, 1, 0, 0, 0, 0]
                     , [0, 0, 0, 0, 0, 1, 0, 0, 0]
                     , [0, 0, 0, 0, 0, 0, 1, 0, 0]
                     , [0, 0, 0, 0, 0, 0, 0, 1, 0]
                     , [0, 0, 0, 0, 0, 0, 0, 0, 1]
                     ])


A1 = ImmutableMatrix([ [u1, 0, 0, 0, 0, 0, 0, 0, 0]
                     , [ rho1 * u1, alpha1 * u1, 0, 0, 0, 0, 0, alpha1 * rho1, 0]
                     , [-rho2 * u1, 0, alpha2 * u1, 0, 0, 0, 0, alpha2 * rho2, 0]
                     , [0, 0, 0, u1, 0, 0, 0, -F11, 0]
                     , [0, 0, 0, 0, u1, 0, 0, -F12, 0]
                     , [0, 0, 0, 0, 0, u1, 0, 0, -F11]
                     , [0, 0, 0, 0, 0, 0, u1, 0, -F12]
                     , [((p1 - p2) - sigma11) / rho, (alpha1 / rho) * (C1**2 - sigma11 / rho1), (alpha2 / rho) * C2**2, -K11, -K12, -K21, -K22, u1, 0]
                     , [-sigma21 / rho, -(alpha1 * sigma21 / rho1) / rho, 0, -L11, -L12, -L21, -L22, 0, u1]
                     ])

Ax = A0.inv() * A1

F = ImmutableMatrix([ [F11, F12, 0]
                    , [F21, F22, 0]
                    , [0,   0,   1]
                    ])

A = F.inv() # Distortion tensor.
G = A.T * A
devG = G - (G[0, 0] + G[1, 1] + G[2, 2]) / 3 * eye(3)
esPart = devG.T * devG

es = (Cs**2 / 4) * (esPart[0, 0] + esPart[1, 1] + esPart[2, 2])

# B = F * F.T
# G = B.inv()
# g = G / pow(G.det(), 1.0 / 3.0)
# g2 = g * g
# J2 = g2[0, 0] + g2[1, 1] + g2[2, 2]
# es = (Cs**2 / 8) * (J2 - 3)

assert es.subs({F11 : 1, F12 : 0, F21 : 0, F22 : 1}) == 0, "Energy isn't correct"

explicit_sigma11 = rho1 * (F11 * diff(es, F11) + F12 * diff(es, F12))
explicit_sigma12 = rho1 * (F21 * diff(es, F11) + F22 * diff(es, F12))
explicit_sigma21 = rho1 * (F11 * diff(es, F21) + F12 * diff(es, F22))
explicit_sigma22 = rho1 * (F21 * diff(es, F21) + F22 * diff(es, F22))

assert (explicit_sigma12 - explicit_sigma21).simplify() == 0, "Shear stress isn't correct"

explicit_K11 = ((alpha1 / rho) * diff(explicit_sigma11, F11)).simplify()
explicit_K12 = ((alpha1 / rho) * diff(explicit_sigma11, F12)).simplify()
explicit_K21 = ((alpha1 / rho) * diff(explicit_sigma11, F21)).simplify()
explicit_K22 = ((alpha1 / rho) * diff(explicit_sigma11, F22)).simplify()

explicit_L11 = ((alpha1 / rho) * diff(explicit_sigma21, F11)).simplify()
explicit_L12 = ((alpha1 / rho) * diff(explicit_sigma21, F12)).simplify()
explicit_L21 = ((alpha1 / rho) * diff(explicit_sigma21, F21)).simplify()
explicit_L22 = ((alpha1 / rho) * diff(explicit_sigma21, F22)).simplify()

subs_list0 = { K11: explicit_K11, K12: explicit_K12, K21: explicit_K21, K22: explicit_K22
             , L11: explicit_L11, L12: explicit_L12, L21: explicit_L21, L22: explicit_L22
             , sigma11: explicit_sigma11, sigma21: explicit_sigma21
             }

subs_list1 = {F11: 1, F12: 0, F21: 0, F22: 1, u1: 0}

ref_K11 = explicit_K11.subs(subs_list1)
ref_K12 = explicit_K12.subs(subs_list1) # 0
ref_K21 = explicit_K21.subs(subs_list1) # 0
ref_K22 = explicit_K22.subs(subs_list1)

ref_L11 = explicit_L11.subs(subs_list1) # 0
ref_L12 = explicit_L12.subs(subs_list1)
ref_L21 = explicit_L21.subs(subs_list1)
ref_L22 = explicit_L22.subs(subs_list1) # 0

ref_sigma11 = explicit_sigma11.subs(subs_list1).simplify()
ref_sigma21 = explicit_sigma21.subs(subs_list1).simplify()

subs_list2 = { F11: 1, F12: 0, F21: 0, F22: 1
             , K11: ref_K11, K12: ref_K12, K21: ref_K21, K22: ref_K22
             , L11: ref_L11, L12: ref_L12, L21: ref_L21, L22: ref_L22
             , sigma11: ref_sigma11, sigma21: ref_sigma21
             , u1 : 0
             }

def state_sub(
    alpha1_: float,
    rho1_: float,
    rho2_: float,
    u1_: float,
    F11_: float,
    F12_: float,
    F21_: float,
    F22_: float,
    C10: float,
    rho10: float,
    p10: float,
    gamma1: float,
    mu: float,
    C20: float,
    rho20: float,
    p20: float,
    gamma2: float,
) -> Dict[Symbol, float]:

    alpha2_ = 1.0 - alpha1_
    rho_ = alpha1_ * rho1_ + alpha2_ * rho2_

    p1_ = p10 + (((C10 ** 2) * rho10) * (pow(rho1_ / rho10, gamma1) - 1.)) / gamma1
    C1_ = sqrt((C10 ** 2) * pow(rho1_ / rho10, gamma1 - 1.))

    p2_ = p20 + (((C20 ** 2) * rho20) * (pow(rho2_ / rho20, gamma2) - 1.)) / gamma2
    C2_ = sqrt((C20 ** 2) * pow(rho2_ / rho20, gamma2 - 1.))

    Cs_ = sqrt(mu / rho10)

    subs_p = {
        alpha1: alpha1_,
        alpha2: alpha2_,
        rho1: rho1_,
        rho2: rho2_,
        rho: rho_,
        p1: p1_,
        p2: p2_,
        C1: C1_,
        C2: C2_,
        Cs: Cs_,
    }
    subs_F = { F11: F11_, F12: F12_, F21: F21_, F22: F22_ }
    subs_u = { u1: u1_ }
    subs_K = {
        K11: explicit_K11.subs(subs_F).subs(subs_p),
        K12: explicit_K12.subs(subs_F).subs(subs_p),
        K21: explicit_K21.subs(subs_F).subs(subs_p),
        K22: explicit_K22.subs(subs_F).subs(subs_p),
    }
    subs_L = {
        L11: explicit_L11.subs(subs_F).subs(subs_p),
        L12: explicit_L12.subs(subs_F).subs(subs_p),
        L21: explicit_L21.subs(subs_F).subs(subs_p),
        L22: explicit_L22.subs(subs_F).subs(subs_p),
    }
    subs_s = {
        sigma11: explicit_sigma11.subs(subs_F).subs(subs_p),
        sigma21: explicit_sigma21.subs(subs_F).subs(subs_p),
    }
    return {**subs_p, **subs_K, **subs_L, **subs_s, **subs_u, **subs_F}


# @TODO: remove
v51 = Symbol('v51')
v52 = Symbol('v52')
v53 = Symbol('v53')
v54 = Symbol('v54')
v55 = Symbol('v55')
v56 = Symbol('v56')
v57 = Symbol('v57')
v58 = Symbol('v58')
v59 = Symbol('v59')

v61 = Symbol('v61')
v62 = Symbol('v62')
v63 = Symbol('v63')
v64 = Symbol('v64')
v65 = Symbol('v65')
v66 = Symbol('v66')
v67 = Symbol('v67')
v68 = Symbol('v68')
v69 = Symbol('v69')

v71 = Symbol('v71')
v72 = Symbol('v72')
v73 = Symbol('v73')
v74 = Symbol('v74')
v75 = Symbol('v75')
v76 = Symbol('v76')
v77 = Symbol('v77')
v78 = Symbol('v78')
v79 = Symbol('v79')

v81 = Symbol('v81')
v82 = Symbol('v82')
v83 = Symbol('v83')
v84 = Symbol('v84')
v85 = Symbol('v85')
v86 = Symbol('v86')
v87 = Symbol('v87')
v88 = Symbol('v88')
v89 = Symbol('v89')

v91 = Symbol('v91')
v92 = Symbol('v92')
v93 = Symbol('v93')
v94 = Symbol('v94')
v95 = Symbol('v95')
v96 = Symbol('v96')
v97 = Symbol('v97')
v98 = Symbol('v98')
v99 = Symbol('v99')
